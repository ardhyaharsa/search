const mix = require('laravel-mix');

mix
    .setPublicPath('./public')
    .styles([
        'node_modules/bulma/css/bulma.css',
        'resources/stylesheet/main.css'
    ], 'public/stylesheet/app.css')
    .js([
        'resources/javascript/main.js'
    ], 'javascript/app.js');
