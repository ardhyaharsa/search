**Simple Search Engine**

Simple Search Engine API from **Kapanlagi Channel RSS Feeds**. I'am using [Nalapa](https://github.com/anpandu/nalapa) to get clean format for Tittle and Description from each Meta URL. Then then I give an assessment based on the suitability from search parameter that has been entered by the User.


**Kapanlagi Channel :**

1. [bola.net](https://www.bola.net/feed/)
2. [kapanlagi.com](https://www.kapanlagi.com/feed/entertainment.xml)
3. [liputan6.com](https://feed.liputan6.com/rss)
4. [merdeka.com](https://www.merdeka.com/feed/)
5. [otosia.com](https://a.otosia.com/rss/)


**Search Parameter :**

1. search
2. limit
3. offset
