const express = require("express");
const compression = require("compression");
const cron = require("cron");
const mongo = require("mongodb");

const app = express();
const port = 3000;
let article = false;

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/public/main.html");
});

app.get("/search", (req, res) => {
  const params = req.query;

  let search_params = value(params.search, "").toLowerCase();
  search_params = search_params.replace(/[^a-z\d\s]+/gi, "");
  var search = pra_processing(search_params);

  const point = {
    title: 1,
    description: 1,
    new: 1
  };
  let limit = Number(value(params.limit, 12));
  const offset = Number(value(params.offset, 0));

  const titles = [], descriptions = [], where_setup = [];
  for (const a in search) {
    const title = {
      title: {
        $regex: search[a],
        $options: "i"
      }
    };
    titles.push(title);

    const description = {
      description: {
        $regex: search[a],
        $options: "i"
      }
    };
    descriptions.push(description);
  }

  where_setup.push({
    original: {
      $regex: search_params,
      $options: "i"
    }
  });

  if(titles.length > 0) {
    where_setup.push({
      $and: titles
    });
  }

  if(descriptions.length > 0) {
    where_setup.push({
      $and: descriptions
    });
  }

  const find = {
    $or: where_setup
  };
  const field = {
    projection: {
      _id: 0,
      image: 0
    }
  };
  article.find(find, field).toArray((err, docs) => {
    docs.reverse();

    const result_count = docs.length;
    for (const a in docs) {
      const doc = docs[a];

      let doc_point = 0;
      doc_point += scoring(search_params, doc.original.title.toLowerCase(), point.title);
      doc_point += scoring(search_params, doc.original.description.toLowerCase(), point.description);
      doc_point += scoring(search, doc.title, point.title);
      doc_point += scoring(search, doc.description, point.description);
      doc_point += ((docs.length - a) / docs.length) * point.new;

      doc.point = doc_point;
    }

    docs.sort(compare);

    if(docs.length < offset) {
      offset = 0;
    }
    else {
      limit = offset + limit;
      if(limit > docs.length)
        limit = docs.length;
    }
    docs = docs.slice(offset, limit);

    const data = {
      search: search,
      result: docs,
      result_count: result_count
    };
    res.json(data);
  });
});

const cronjob = cron.CronJob;
const cron_fetch = new cronjob("0 */5 * * * *", () => {
  const fetchs = [
    {
      channel: "bolanet",
      url: [
        "https://www.bola.net/feed/"
      ]
    },
    {
      channel: "kapanlagi",
      url: [
        "https://www.kapanlagi.com/feed/entertainment.xml"
      ]
    },
    {
      channel: "liputan6",
      url: [
        "https://feed.liputan6.com/rss",
        "https://www.liputan6.com/bisnis/feed/rss",
        "https://www.liputan6.com/health/feed/rss",
        "https://www.liputan6.com/lifestyle/feed/rss",
        "https://www.liputan6.com/tekno/feed/rss",
        "https://www.liputan6.com/showbiz/feed/rss",
        "https://www.liputan6.com/bola/feed/rss",
        "https://www.liputan6.com/otomotif/feed/rss"
      ]
    },
    {
      channel: "merdeka",
      url: [
        "https://www.merdeka.com/feed/"
      ]
    },
    {
      channel: "otosia",
      url: [
        "https://a.otosia.com/rss/"
      ]
    }
  ];

  for (const a in fetchs) {
    const fetch = fetchs[a];
    for (const b in fetch.url) {
      const url = fetch.url[b];
      fetch_data(fetch.channel, url);
    }
  }
});
cron_fetch.start();

app.use(express.static(__dirname + "/public"));
app.use(compression());
app.disable("x-powered-by");

const mongo_config = {
  useUnifiedTopology: true,
  useNewUrlParser: true
};
mongo.MongoClient.connect("mongodb://localhost:27017", mongo_config, (err, database) => {
  if(err) throw err;

  article = database.db("kly").collection("article");
  app.listen(port, () => {
    console.log("Server running on port " + port);
  });
});

const unique = (value, index, self) => {
  return self.indexOf(value) === index;
}

const pra_processing = (string) => {
  const nalapa = require("nalapa");
  const tokenizer = nalapa.tokenizer;
  const word = nalapa.word;
  const cleaner = nalapa.cleaner;

  const token = [];
  const tokenizes = tokenizer.tokenize(string.replace(/[^a-z\d\s]+/gi, "").toLowerCase());
  for (const a in tokenizes) {
    const tokenize = cleaner.removeNonAlphaNumeric(tokenizes[a]);
    if(!word.isStopword(tokenize) && tokenize.length > 0)
      token.push(word.stem(tokenize));
  }

  return token.filter( unique );
}

const fetch_data = (channel , url) => {
  const fetch = require("fetch");
  fetch.fetchUrl(url,(error, meta, body) => {
    if (typeof body !== "undefined") {
      const xml2js = require("xml2js");
      xml2js.parseString(body.toString() , (err, result) => {
        if (typeof result !== "undefined") {
          const items = result.rss.channel[0].item;
          items.reverse();

          for(const a in items) {
            const item = items[a];

            const metafetch = require("metafetch");
            metafetch.fetch(item.link[0]).then((meta) => {
              const title = pra_processing(meta.title);
              const description = pra_processing(meta.description);

              const data = {
                url: meta.url,
                channel: channel,
                original: {
                  title: meta.title,
                  description: meta.description
                },
                title: title.join(" "),
                description: description.join(" "),
                image: meta.meta["og:image"]
              };

              const where = {
                url: meta.url
              };
              article.find(where).toArray((err, docs) => {
                if (docs.length > 0) {
                  const doc = docs[0];
                  const where = {
                    _id: doc._id
                  };
                  const update = {
                    $set: data
                  };
                  article
                    .updateOne(where, update)
                    .catch(console.error);

                  if(docs.length > 1) {
                    const where_setup = [];
                    for (var b = 1; b < docs.length; b++) {
                      const doc = docs[b];
                      where_setup.push({
                        _id: doc._id
                      });
                    }

                    const where = {
                      $or: where_setup
                    };
                    article
                      .deleteMany(where)
                      .catch(console.error);
                  }
                }
                else {
                  article
                    .insertOne(data)
                    .catch(console.error);
                }
              });
            }).catch(console.error);
          }
        }
      });
    }
  });
}

const scoring = (from , search , point) => {
  let count = 0;
  from = Array.isArray(from) ? from : [from];

  for(const a in from) {
    if(search.indexOf(from[a]) != -1)
      count++;
  }

  return count * point;
}

const compare = (a , b) => {
  if (a.point < b.point)
    return 1;

  if (a.point > b.point)
    return -1;

  return 0;
}

const value = (value , default_value) => {
  if (typeof value === "undefined")
    value = default_value;

  return value;
}
