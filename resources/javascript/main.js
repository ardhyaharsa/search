let process = true, total = -1;

document.querySelector("input[name=search]").addEventListener("change", () => {
  search(true);
});

window.addEventListener("scroll", () => {
  if(((window.innerHeight + window.scrollY) / document.body.offsetHeight) >= 0.875) {
    if(total != document.querySelectorAll(".search-result .column").length) search(false);
  }
});

const search = (restart) => {
  const mobile = window.innerWidth <= 768;
  const search = document.querySelector("input[name=search]").value;
  const offset = restart ? 0 : document.querySelectorAll(".search-result .column").length;
  const search_process = document.querySelector(".search-process");

  if(process) {
    process = false;
    search_process.classList.remove("hide");

    const request = new XMLHttpRequest();
    request.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        const data = JSON.parse(this.responseText);

        document.querySelector(".processing").innerHTML = "";
        for(const a in data.search) {
          const search = data.search[a];
          let tag = document.createElement("span");
          let tags_class = ["tag","is-primary"];
          for(b = 0 ; b < tags_class.length ; b++) {
            tag.classList.add(tags_class[b]);
          }
          tag.innerHTML = search;

          document.querySelector(".processing").appendChild(tag);
        }

        if (restart) {
          document.body.scrollTop = document.documentElement.scrollTop = 0;
          document.querySelector(".search-result").innerHTML = "";
        }

        total = data.result_count;
        document.querySelector(".result-count b").innerHTML = formatNumber(total);
        for (const a in data.result) {
          var result = data.result[a];
          var selector = document.querySelector(".search-master");
          selector.querySelector("a").setAttribute("href", result.url);

          selector.querySelector(".content .title").innerHTML = result.original.title;
          selector.querySelector(".content .subtitle").innerHTML = result.original.description;
          selector.querySelector(".card-footer-item span.channel").innerHTML = result.channel;

          let search = document.createElement("div");
          let searchs_class = ["column","is-half-tablet","is-one-third-widescreen","is-one-quarter-fullhd"];
          for(const b in searchs_class) {
            search.classList.add(searchs_class[b]);
          }

          search.innerHTML = selector.innerHTML;
          document.querySelector(".search-result").appendChild(search);
        }

        const height = window.getComputedStyle(document.querySelector(".section.search-bar")).height;
        document.querySelector("body").style.minHeight = "calc(100vh - " + height + ")";
        document.querySelector(".section.search-res").style.marginTop = "calc(" + height + " - .75rem)";

        process = true;
        search_process.classList.add("hide");
      }
    };

    const params = {
      search: search,
      limit: mobile ? 12 : 24,
      offset: offset
    };
    request.open("GET", "/search?" + serialize(params));
    request.send();
  }
}

const serialize = (obj) => {
  const str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }

  return str.join("&");
}

const formatNumber = (num) => {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

search(true);
